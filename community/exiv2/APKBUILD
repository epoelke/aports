# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=exiv2
pkgver=0.27.3
pkgrel=2
pkgdesc="Exif and Iptc metadata manipulation library and tools."
url="https://exiv2.org"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0-or-later"
depends_dev="expat-dev zlib-dev"
makedepends="$depends_dev cmake"
subpackages="$pkgname-dev $pkgname-doc"
source="https://exiv2.org/builds/exiv2-$pkgver-Source.tar.gz
	CVE-2021-3482.patch
	CVE-2021-29457.patch
	CVE-2021-29458.patch
	CVE-2021-29463.patch
	CVE-2021-29470.patch
	CVE-2021-29473.patch
	CVE-2021-29623.patch
	CVE-2021-32617.patch"
builddir="$srcdir"/$pkgname-$pkgver-Source

prepare() {
	default_prepare

	# -fcf-protection=full is only usable on x86_64 and x86 since those
	# are the arches where Intel's CET Control-Flow Enforcement Technology
	# is available
	case "$CARCH" in
		x86_64|x86) ;;
		*) sed -i 's| -fcf-protection||g' cmake/compilerFlags.cmake ;;
	esac
}

# secfixes:
#   0.27.3-r2:
#     - CVE-2021-29463
#     - CVE-2021-29470
#     - CVE-2021-29473
#     - CVE-2021-29623
#     - CVE-2021-32617
#   0.27.3-r1:
#     - CVE-2021-3482
#     - CVE-2021-29457
#     - CVE-2021-29458
#   0.27.2-r6:
#     - CVE-2019-20421
#   0.27.2-r2:
#     - CVE-2019-17402
#   0.27.2-r0:
#     - CVE-2019-13108
#     - CVE-2019-13109
#     - CVE-2019-13110
#     - CVE-2019-13111
#     - CVE-2019-13112
#     - CVE-2019-13113
#     - CVE-2019-13114

build() {
	cmake -B build . \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DEXIV2_BUILD_SAMPLES=OFF
	make -C build
}

check() {
	make -C build test
}

package() {
	make -C build DESTDIR="$pkgdir" install
}

sha512sums="
3f5758ee862b811eeb89cc75fc2bbd8bf10329efa2ce1e68555cdc7729faa6cfd1603e0cc859fbdbe6d8fd5e53bd9b9e6d869d8a20ed17497bf87ce78c005de9  exiv2-0.27.3-Source.tar.gz
9c3f52881df19ca2be9a66480642f5fb91d4be0dd3e513902ec4336536736ec37447df452a775dd933dc011de9d8c4ca3760e3c7545ab5d150876d9dab64bd06  CVE-2021-3482.patch
27c187c1eb1b3118e8dc3342c1ba7700a522f6e05083b3a4540c2490580320f3e59eb06a1f6abc98ad28f6a3ccb91351ee56caaa9befbb2dc6c5ea1567c711f2  CVE-2021-29457.patch
d892622c53b1ba5ac48c33a599f37853c8471cce82ae14679d33df17f1251b2e353ce2436c1c730a9a8d1e8bfd9175f7b79263e9b402ecf3ac024db69d10c0e0  CVE-2021-29458.patch
e885b5bb23b317a7050869b2bcb244fd8744ce2f6019285c8bf5dc2f16c37cb4fc76b9e214fe0fff14529bd49c63d968c8c04908841c5c9cf74c5bb9a6eb9363  CVE-2021-29463.patch
5fac2406f45439983cc6334968c238952e6ce1d84b6f665114a2dfee306bbafc02279ebcb6bda06dd22ac47117496126d9e2e1ff9882e0c03794bce781670c33  CVE-2021-29470.patch
fcb726d7fb2de1709f7f7a43ceb55b29c18c3f55f4872671e13874af219c563a05aa0d6b87c7e86b9e544d618e9ea3638cf3e830f597587b5ae0736222cccc80  CVE-2021-29473.patch
fbe5daabbcedd40474d4a5bdf79886408325768dd3cd81ab0f6332efca48e26239a600ff770788461b825146e2ad97ec334f74ea8e2d4121e8204bd95f3bfabb  CVE-2021-29623.patch
565a43fb1fef12d82adc77f01ffbb90a410c8061b1cf20e1fe95faece9727274ebaa3ba3b50c4d08a6e8e7b9b5daf624198d32b02e9d88c2b8f01bf93bd9046b  CVE-2021-32617.patch
"
